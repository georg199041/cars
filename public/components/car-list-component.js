const CarListComponent = (props) => (
    <div>
        <div>Список авто</div>
        {props.cars.map(car => <CarElementComponent car={car} key={car._id} deleteCar={props.deleteCar} />)}
    </div>
);
