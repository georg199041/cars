const carService = {
    getDefaultCars: () => fetch('/cars').then(res => res.json()).catch(e => console.log(e)),
    postCars: (data) => fetch('/car', { method: 'POST', headers: {
        'Content-Type': 'application/json'
    }, body: JSON.stringify(data)}),
    updateCars: (data) => fetch(`/car/${data._id}`, { method: 'PUT', headers: {
        'Content-Type': 'application/json'
    }, body: JSON.stringify(data)}),
    deleteCars: (id) => fetch(`/car/${id}`, { method: 'DELETE', headers: {
        'Content-Type': 'application/json'
    }})
}