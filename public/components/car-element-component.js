class CarElementComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showEditForm: false,
            car: this.props.car
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            car: {
                _id: this.props.car._id,
                make: this.editForm.make.value,
                year: this.editForm.year.value,
                model: this.editForm.model.value,
                trim: this.editForm.trim.value,
                bodyType: this.editForm.bodyType.value,
                color: this.editForm.color.value
            }
        }, () => carService.updateCar(this.state.car));
    }

    render() {
        return (
            <div className="car-element">
                <div onClick={() => this.setState({ showEditForm: !this.state.showEditForm })}>
                    Елемент списку авто
                    <div>Назва: {this.props.car.title}</div>
                    <div>Рік випуску: {this.props.car.year}</div>
                    <div>Жанр: {this.props.car.genre}</div>
                    <div>Рейтинг: {this.props.car.rate}</div>
                    <div>Опис: {this.props.car.desc}</div>
                </div>
                <button onClick={() => this.props.deleteCar(this.props.car._id)}>Видалити</button>  
        
                {this.state.showEditForm ? <form id="car-edit" name="car" ref={el => this.editForm = el} onSubmit={e => this.handleSubmit(e)}>
                    <input type="text" name="title" defaultValue={this.state.car.title} />
                    <input type="text" name="year" defaultValue={this.state.car.year} />
                    <input type="text" name="genre" defaultValue={this.state.car.genre} />
                    <input type="text" name="rate" defaultValue={this.state.car.rate}/>
                    <textarea name="desc" defaultValue={this.state.car.desc}></textarea>
                    <button>Оновити</button>
                </form> : null}
            </div>
        )
    }
};


