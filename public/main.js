class MainComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      cars: []
    }
  }

  componentDidMount() {
    carService.getDefaultCars().then(data => this.setState({ cars: data }));
  }

  deleteCar(id) {
    this.setState({
      cars: this.state.movies.filter(item => item._id !== id)
    });
    movieService.deleteMovie(id);
  }

  render() {
    return (
      <div>
        <div>Довідник по сучасним автомобілям</div>
        <CarAdd />
        <CarListComponent cars={this.state.cars} deleteCar={this.deleteCar.bind(this)} /> 
      </div>
      
    )
  }
}