class CarAdd extends React.Component {
    constructor() {
        super();
        this.state = {
            showCarAdd: false,
            jsonError: false
        };
    }

    submitMovies() {
        try {
            let hopeValidJson = null;
            this.setState({ jsonError: false });
            if (this.jsonCar.value) {
                hopeValidJson = JSON.parse(this.jsonCar.value.toString());
                carService.postMovie(hopeValidJson);
            } else {
                carService.postMovie({ title: this.form.title.value, year: this.form.year.value, genre: this.form.genre.value, rate: this.form.rate.value, desc: this.form.desc.value });
            }
        } catch(e) {
            this.setState({ jsonError: e });
        }
    }
    render() {
        return (
            <div>
                <button onClick={() => this.setState({ showCarAdd: !this.state.showCarAdd })}>Форма додавання</button>
                {this.state.showCarAdd ? <div><form id="car-add" name="car-add" ref={fields => this.form = fields}>
                    <label htmlFor="make">Виробник</label><input type="text" name="make" />
                    <label htmlFor="year">рік випуску</label><input type="text" name="year" />
                    <label htmlFor="model">модель</label><input type="text" name="model" />
                    <label htmlFor="trim">комплектація</label><input type="text" name="trim" />
                    <label htmlFor="bodyType">комплектація</label><input type="text" name="bodyType" />
                    <label htmlFor="color">колір</label><input type="text" name="color" />
                </form>
                <div>або</div>
                <label htmlFor="bulk">Додати багато машин одразу у json</label>
                <textarea name="bulk" ref={data => this.jsonCars = data}></textarea>
                {this.state.jsonError ? <div>Будь ласка перевірте валідність введеного JSON {this.state.jsonError}</div> : null}
                <button  onClick={() => this.submitMovies()}>Відправити</button></div> : null}
            </div>
        );
    }
}