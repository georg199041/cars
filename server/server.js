const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
const mongo = require('./db-client');

// serve static
const init = async () => {
    const server = new Hapi.Server({
        port: 3000,
        routes: {
            files: {
                relativeTo: Path.join(__dirname, '../public')
            }
        }
    });
    await server.register(Inert);
    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true
            }
        }
    });

    server.route({  
        method: 'GET',
        path: '/cars',
        handler: async() => await mongo().read()
    });

    server.route({  
        method: 'GET',
        path: '/car/{id}',
        handler: async(request) => await mongo().read(request.params.id)
    });

    server.route({  
        method: 'POST',
        path: '/car',
        handler: async(request) => await mongo().create(request.payload)
    });

    server.route({  
        method: 'PUT',
        path: '/car/{id}',
        handler: async(request) => await mongo().update(request.params.id, request.payload)
    });

    server.route({  
        method: 'DELETE',
        path: '/car/{id}',
        handler: async(request) => await mongo().delete(request.params.id)
    });

    await server.start();
    await mongo().connect();
    console.log('Server running at:', server.info.uri);
    // listen on SIGINT signal and gracefully stop the server and close db connection
    process.on('SIGINT', () => {  
        console.log('stopping hapi server')
        mongo().disconnect();
        server.stop({ timeout: 10000 }).then(function (err) {
            console.log('hapi server stopped')
            process.exit((err) ? 1 : 0)
        });
    });
};

init();

